%include "lib.inc"

section .text
global find_string


find_string:
	push r13
	push r14
	mov r13, rdi
	mov r14, rsi
	.loop:
		test r14, r14
		jz .not_found
		mov rdi, r13
		lea rsi, [r14 + 8]
		call string_equals
		test rax, rax
		jnz .found
		mov r14, [r14]
		jmp .loop
	.found:
		mov rax, r14
		jmp .end
	.not_found:
		xor rax, rax
	.end:
		pop r14
		pop r13
		ret
		