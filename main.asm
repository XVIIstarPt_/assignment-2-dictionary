%include "lib.inc"
%include "colon.inc"
%include "words.inc"
extern find_string
%define BUFFER_SIZE 256
%define BYTE_SIZE 8

section .rodata
	message_not_found: db "Element not found", 0
	message_invalid_input: db "Invalid input", 0
section .bss
	buffer: resb BUFFER_SIZE
section .text
global _start:
_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
	test rax, rax
	jz .invalid_input
	push rdx
	mov rdi, buffer
	mov rsi, NEXT_KEY
	call find_string
	pop rdx
	test rax, rax
	jz .not_found
	lea rdi, [rax + BYTE_SIZE + rdx + 1]
	call print_out
	jmp .end
	
	.not_found:
		mov rdi, message_not_found
		call print_err
		jmp .end
	.invalid_input:
		mov rdi, message_invalid_input
		call print_err
	.end:
		call exit

