ASM = nasm
ASMFLAGS = -f elf64 -o
LINK = ld
LINKFLAG = -o
PYTHON = python3

.PHONY: all clean test

all: program

clean: program
	@rm *.o program

test: program
	$(PYTHON) test.py

%.o: %.asm
	$(ASM) $(ASMFLAGS) $@ $<

program: main.o dict.o lib.o
	$(LINK) $(LINKFLAG) $@ $^
