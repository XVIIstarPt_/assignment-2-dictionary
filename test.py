import subprocess
from subprocess import Popen, PIPE

INPUT_STRS = [
        "first", "second", "third", "fourth", "fifth", "One", "must", "imagine", "Sisyphus", "happy", "last", "word", 
        ".", ",", "", "1", "2", "01100010011001010110110001101111011101100110110001100001011100110110101101100001",
        "Kamiltonian cycle", "starplatinum", "bebra" * 69, "lo" * 228, "trololo", "6D 6F 72 74 75 6D", 
        "6E79617073696C6F6E", "MIKHEEV HOLDINGS LTD.", "ZA WARUDO", "\t", "\t \t", "\t\t DecafBanana \t\t\t"
        ]

TESTS_COUNT = len(INPUT_STRS)

DICTIONARY = {
        "first": "One",
        "second": "must",
        "third": "imagine",
        "fourth": "Sisyphus",
        "fifth": "very happy"
}

MATCHING_INFORMATION = False


def check(input_str):
    return DICTIONARY.get(input_str, "")


def print_test_data(stdin, stdout, stderr):
    print(f"stdin: '{stdin}'")
    print(f"stdout: '{stdout}'")
    print(f"stderr: '{stderr}'")

    
if __name__ == "__main__":
    print("Tests launching...\n")

    command = './program'
    wrong_count = 0
    for input_str in INPUT_STRS:
        proc = Popen(command, stdin=PIPE, stdout=PIPE, stderr=PIPE, encoding='utf-8')
        stdout, stderr = proc.communicate(input=input_str + "\n")
        right_stdout = check(input_str)
        if stdout != right_stdout:
            wrong_count += 1
            print("Wrong answers:")
            print_test_data(input_str, stdout, stderr)
            print(f"expected stdout: '{right_stdout}'")
            print('=-' * 50)
        elif MATCHING_INFORMATION:
            print("Matching return")
            print_test_data(input_str, stdout, stderr)
            print('=-' * 25, '=')
        
        print("\nTests finished.")
        if wrong_count == 0:
            print(f"OK ({TESTS_COUNT}/{TESTS_COUNT})")
        else:
            print(f"Wrong returns: {wrong_count}")
            print(f"Total tests: {TESTS_COUNT}")
            exit(1)
